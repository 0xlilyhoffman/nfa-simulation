"""

Automata pa2.py
Spring 2017
Lily Hoffman

Simulates the computation of a NFA on various input strings
NFA configurations (states, alphabet, transition function, start state,  accepting states)
as well as input strings are read in from a file
Program displays to user whether the input string was accepted or rejected by the NFA

This program is the non-deterministic counterpart of the DFA Simulator.
Program input and output are the same, except for now the transition function can contain "epsilon transitions" (transitions that proceed without reading new input values) as well as multiple transitions out of a single state for a single input value

"""


import sys
import re



def parseNFAInfile(infile):
    f = open(infile)

    #States
    num_states = int(f.readline())

    #Alphabet
    alphabet = f.readline().strip()


    # Transition function = list of 'q1 a q2' elements
    transition_function = list()

    reading_transition_function = True
    while (reading_transition_function):
        line = f.readline()
        if len(line) < 3:
            reading_transition_function = False
        else:
            transition_function.append(line)

    # Create transition table mapping (current_state, current_symbol) -> next_state
    transition_table = {}
    for transition in transition_function:
        regex_match = re.match("(\d+) '(.)' (\d+)", transition)
        in_state = int(regex_match.group(1))
        reading_symbol = regex_match.group(2)
        to_state = int(regex_match.group(3))
        # transition_table[(in_state, reading_symbol)] = to_state
        if transition_table.get((in_state, reading_symbol)) == None:
            transition_table[(in_state, reading_symbol)] = list()
            transition_table.get((in_state, reading_symbol)).append(to_state)
        else:
            transition_table.get((in_state, reading_symbol)).append(to_state)

    # Start state = int
    start_state = int(f.readline().strip())

    #Accept state = list of ints
    accept_states = list()
    accept_states_line = f.readline().strip()
    accept_states_line = str(accept_states_line).split()
    for state in accept_states_line:
        if state != ' ' and state != '\n':
            accept_states.append(int(state))

    return(num_states, alphabet, transition_table, start_state, accept_states)




def convertToDFA():
    """
    Converts the NFA from the infile to a DFA
    :return: 5-tuple DFA description
    """
    num_states, alphabet, transition_table, start_state, accept_states = parseNFAInfile(sys.argv[1])

    # Alphabet
    DFA_alphabet = alphabet

    # Initial state
    DFA_start_states = determineStartState(start_state, transition_table)

    # Transition Function
    DFA_transitions, DFA_transition_map, DFA_accept_states = determineTransitionFunction(DFA_start_states, transition_table, alphabet, accept_states)

    # Start state (formatted)
    DFA_start_state = DFA_transition_map[tuple((DFA_start_states))]

    # Number of States
    DFA_num_states = len(DFA_transitions) / len(alphabet)

    return (DFA_num_states, DFA_alphabet, DFA_transitions, DFA_start_state, DFA_accept_states)

def determineStartState(start_state, transition_table):
    """
    Determines start state of the DFA
    :param start_state: initial state in NFA
    :param transition_table: NFA transition function
    :return: DFA_start_states = list of the states the NFA could begin in
    """

    #Start state = NFA start state + states with epsilon transitions from start states
    DFA_start_states = list()
    DFA_start_states.append(start_state)
    if(start_state, "e") in transition_table:
        DFA_start_states.append(transition_table.get((start_state, "e")))
    for q in DFA_start_states:
        if type(q) is list:
            q = q[0]
        if(q, "e") in transition_table:
            DFA_start_states.append(transition_table.get((q, "e")))

    #Clean up format of DFA_start_state
    DFA_start_states_clean = list()
    for element in DFA_start_states:
        if type(element) is list:
            for item in element:
                DFA_start_states_clean.append(item)
        else:
            DFA_start_states_clean.append(element)

    DFA_start_states = DFA_start_states_clean
    DFA_start_states = list(set(DFA_start_states))
    DFA_start_states.sort()
    return DFA_start_states

def determineTransitionFunction(DFA_start_states, transition_table, alphabet, accept_states):
    """
    Determines the transition function of the DFA using that of the NFA
    :param DFA_start_states: NFA start state + (start -> epsilon) destinations
    :param transition_table: NFA transition table
    :param alphabet: DFA/NFA alphabet
    :param accept_states: NFA accept states
    :return: DFA transition function, DFA transition mapping to clean up format of output, DFA accept states
    """

    #Determine initial transitions
    DFA_transitions = determineInitialTransition(DFA_start_states, alphabet, transition_table)

    #Run machine
    DFA_transitions, DFA_accept_states = runMachine(DFA_transitions, alphabet, transition_table, accept_states)


    # Clean up format of transition function
    DFA_transitions, DFA_transition_map = formatTransitionFunction(DFA_transitions, alphabet)

    #Determine accept state
    new_DFA_accept_states = list()
    for state in DFA_accept_states:
        new_accept_state = DFA_transition_map.get(tuple(state))
        new_DFA_accept_states.append(new_accept_state)
    DFA_accept_states = list(set(new_DFA_accept_states))


    return DFA_transitions, DFA_transition_map, DFA_accept_states


def determineInitialTransition(DFA_start_states, alphabet, transition_table):
    """
    Determines initial transition of the DFA based on the initial transitions of the NFA
    :param DFA_start_states: The set of states the NFA could be in
    :param alphabet: NFA/DFA alphabet
    :param transition_table: NFA transition table
    :return: Initial transitions of the DFA inside list: DFA_transitions
    """
    # Initial transition
    DFA_transitions = list()
    for state in DFA_start_states:
        for symbol in alphabet:
            # Determine next state for current state / symbol
            dest = transition_table.get((state, symbol))
            tx = Transition(DFA_start_states, symbol, dest)

            # Account for possibility of no NFA transition out of "state" for "symbol"
            none = False
            if dest == None:
                none = True

            # Search for existing (state, symbol) entry and replace with newly extended set of destinations
            for transition in DFA_transitions:
                if transition.start == tx.start and transition.symbol == tx.symbol and transition.dest != tx.dest:
                    DFA_transitions.remove(transition)
                    # Account for none cases. Else append destination SET.
                    if (tx.dest == None):
                        dest = transition.dest
                    if (transition.dest == None):
                        dest = tx.dest
                    if (tx.dest != None and transition.dest != None):
                        dest = tx.dest + transition.dest
                    dest.sort()
                    tx = Transition(tx.start, tx.symbol, dest)
                    DFA_transitions.append(tx)  ##########################sketch

            # Account for epsilon transitions out of destination
            if (none == False):
                for s in tx.dest:
                    if (s, "e") in transition_table:
                        tx.dest = tx.dest + transition_table.get((s, "e"))
                        tx.dest = list(set(tx.dest))
                DFA_transitions.append(tx)

    #Remove duplicates
    DFA_transitions = removeDuplicates(DFA_transitions)

    return DFA_transitions

def runMachine(DFA_transitions, alphabet, transition_table, accept_states):
    """
    Simulates the machine transitioning through each state for each symbol in the alphabet
    :param DFA_transitions: Transition function of the DFA
    :param alphabet: DFA/NFA alphabet
    :param transition_table: Transition function of the NFA
    :param accept_states: Accepting states of the NFA
    :return: DFA_transitions: Transition function of the DFA based on which state the NFA could be in for any point in the computation.
            DFA_accept_states: Set of accepting states of the DFA based on accepting configurations of the NFA
    """
    # Run machine
    for transition in DFA_transitions:
        already_analyzed = False
        tx_exists = False

        # Determine current state
        current_state = transition.dest

        # Check if this state has already been analyzed
        for transition in DFA_transitions:
            if current_state == transition.start:
                already_analyzed = True
                break
        if (already_analyzed):
            continue

        # Determine next DFA state for each symbol
        for symbol in alphabet:
            for part in current_state:

                # Get next state from single state in the set of states the NFA could be in
                nfa_dest = transition_table.get((part, symbol))

                # Account for epsilon transitions
                if nfa_dest == None:
                    continue
                for single_dest in nfa_dest:
                    if type(single_dest) is list:
                        single_dest = single_dest[0]
                    if (single_dest, "e") in transition_table:
                        nfa_dest_clean = list()
                        nfa_dest.append(transition_table.get((single_dest, "e")))

                        # Clean up formatting
                        for element in nfa_dest:
                            if type(element) is list:
                                for item in element:
                                    nfa_dest_clean.append(item)
                            else:
                                nfa_dest_clean.append(element)
                        nfa_dest = nfa_dest_clean
                        nfa_dest = list(set(nfa_dest))
                        nfa_dest.sort()

                #Add transition to DFA_transitions accordingly
                if nfa_dest != None:
                    nfa_dest = list(set(nfa_dest))
                    nfa_dest.sort()
                    tx = Transition(current_state, symbol, nfa_dest)

                    #... add transitions to DFA_transitions
                    for transition in DFA_transitions:
                        if transition == tx:
                            tx_exists = True
                            break

                        # Search for existing (state, symbol) entry and replace with newly extended set of destinations
                        if transition.start == tx.start and transition.symbol == tx.symbol and transition.dest != tx.dest:
                            extended_dest = list(set((tx.dest + transition.dest)))
                            extended_dest.sort()

                            # Remove old incomplete transition
                            DFA_transitions.remove(transition)

                            # Redefine new transition
                            tx = Transition(tx.start, tx.symbol, extended_dest)
                            break
                    # Append new transition
                    if tx_exists == False:
                        DFA_transitions.append(tx)

    # Remove duplicates - potentially remove?
    for i in range(len(DFA_transitions) - 1):
        try:
            for transition in DFA_transitions:
                if transition.start == DFA_transitions[i].start and transition.symbol == DFA_transitions[
                    i].symbol and transition.dest != DFA_transitions[i].dest:
                    if (len(transition.dest) > len(DFA_transitions[i].dest)):
                        DFA_transitions.remove(DFA_transitions[i])
                    else:
                        DFA_transitions.remove(transition)
        except IndexError:
            break

    # Remove duplicates
    DFA_transitions = removeDuplicates(DFA_transitions)

    #Account for reject states
    DFA_transitions = accountForRejectStates(DFA_transitions, alphabet)

    DFA_accept_states = list()
    for DFAstate in DFA_transitions:
        for NFAstate in accept_states:
            if type(DFAstate.start) == list and NFAstate in DFAstate.start and DFAstate.start not in DFA_accept_states:
                DFA_accept_states.append(DFAstate.start)
            else:
                if NFAstate == DFAstate.start:
                    DFA_accept_states.append(DFAstate.start)


    # Remove duplicates
    DFA_transitions = removeDuplicates(DFA_transitions)

    DFA_transitions.sort()

    # Remove duplicates
    for i in range(len(DFA_transitions) - 1):
        try:
            if DFA_transitions[i] == DFA_transitions[i + 1]:
                DFA_transitions.remove(DFA_transitions[i])
        except IndexError:
            break





    DFA_accept_states = list()
    for DFAstate in DFA_transitions:
        for NFAstate in accept_states:
            if type(DFAstate.start) == list and NFAstate in DFAstate.start and DFAstate.start not in DFA_accept_states:
                DFA_accept_states.append(DFAstate.start)
            else:
                if NFAstate == DFAstate.start:
                    DFA_accept_states.append(DFAstate.start)





    return DFA_transitions, DFA_accept_states


def accountForRejectStates(DFA_transitions, alphabet):
    """
    Include extra state for blocked computations of the NFA
    Blocked computations of an NFA == Reject state of a DFA
    :param DFA_transitions: Transition function (list) of the DFA
    :param alphabet: NFA/DFA alphabet
    :return: Updated DFA transition function
    """
    # Step 1: Convert list of transitions to map of (start, symbol)->dest
    DFA_transitions_lookup = {}
    for transition in DFA_transitions:
        DFA_transitions_lookup[(tuple(transition.start), transition.symbol)] = transition.dest

    # Step 2: Ensure each state has a transition defined for each letter
    reject_state_exists = False
    DFA_states = [transition.start for transition in DFA_transitions]
    i = 0
    for state in DFA_states:
        for symbol in alphabet:
            i += 1
            if DFA_transitions_lookup.get((tuple(state), symbol)) == None:
                reject_state_exists = True
                tx = Transition(state, symbol, [0])
                DFA_transitions.insert(int(i / 2), tx)
                i += 2

    # Step 3: Add transitions from reject state to itself
    if reject_state_exists:
        for symbol in alphabet:
            DFA_transitions.append(Transition([0], symbol, [0]))
    return DFA_transitions


def formatTransitionFunction(DFA_transitions, alphabet):
    """
    Format the states of the DFA to label them 1-n
        ... as opposed to the set of states the NFA could be in

    :param DFA_transitions: DFA transition function
    :param alphabet: DFA/NFA alphabet
    :return: Updated & re-formatted DFA transition function and map that made it
    """
    # Clean up format of transition function
    DFA_transition_map = {}
    DFA_transition_map[(0,)] = 0
    i = 1

    for transition in DFA_transitions:
        DFA_transition_map[tuple(transition.start)] = int(i)
        i += 1 / len(alphabet)

    for transition in DFA_transitions:
        transition.start = DFA_transition_map.get(tuple(transition.start))
        transition.symbol = str("'" + transition.symbol + "'")
        transition.dest = DFA_transition_map.get(tuple(transition.dest))


    return DFA_transitions, DFA_transition_map

def removeDuplicates(DFA_transitions):
    """
    Utility function: removes duplicates in DFA transition function
    :param DFA_transitions: DFA transition function
    :return: Updated DFA transition function
    """
    #Remove duplicates
    for i in range(len(DFA_transitions) - 1):
        for j in range(len(DFA_transitions) -1):
            try:
                for transition in DFA_transitions:
                    if DFA_transitions[j].start == DFA_transitions[i].start and DFA_transitions[j].symbol == DFA_transitions[i].symbol and DFA_transitions[j].dest != DFA_transitions[i].dest:
                        if(len(DFA_transitions[j].dest) > len(DFA_transitions[i].dest)):
                            DFA_transitions.remove(DFA_transitions[i])
                        else:
                            DFA_transitions.remove(DFA_transitions[j])
                    if DFA_transitions[j].start == DFA_transitions[i].start and DFA_transitions[j].symbol == DFA_transitions[
                        i].symbol and DFA_transitions[j].dest == DFA_transitions[i].dest and i != j:
                        if transition == DFA_transitions[i]:
                            DFA_transitions.remove(transition)
            except IndexError:
                break

    return DFA_transitions



def write_to_file():
    """
    Writes DFA to file as
        number of states
        alphabet
        transition function: (current state) 'symbol' (next state)
        start state
        accept states
    :return: nada
    """
    DFA_num_states, DFA_alphabet, DFA_transitions, DFA_start_state, DFA_accept_states = convertToDFA()
    outfile = sys.argv[2]

    f = open(outfile, 'w')
    f.write(str(int(DFA_num_states)))
    f.write("\n")
    f.write(DFA_alphabet)
    f.write("\n")
    for transition in DFA_transitions:
        f.write(str(transition.start) + " " + transition.symbol + " " + str(transition.dest))
        f.write("\n")
    f.write(str(DFA_start_state))
    f.write("\n")
    for state in DFA_accept_states:
        f.write(str(state) + " ")





class Transition:
    """
    Represents a dfa transition
    """
    def __init__(self, start, symbol, dest):
        self.start = start
        self.symbol = symbol
        self.dest = dest

    def __eq__(self, other):
        if self.start == other.start and self.symbol == other.symbol and self.dest == other.dest:
            return True

    def __lt__(self, other):
        if self.start < other.start:
            return True

if __name__ == "__main__":
    print("\n\n\n\n\n**************************************************************************")
    if len(sys.argv) != 3:
        print("Usage: python pa2.py [infile] [outfile]")
    else:
        write_to_file()
        #convertToDFA()


