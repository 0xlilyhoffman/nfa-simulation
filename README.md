# Non-deterministic Finite Automaton Simulation
This project is the non-deterministic counterpart of the DFA project. 
Program input and output are the same, except for now the transition function can contain "epsilon transitions" (transitions that proceed without reading new input values) as well as multiple transitions out of a single state for a single input value